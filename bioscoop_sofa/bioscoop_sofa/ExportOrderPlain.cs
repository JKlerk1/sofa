﻿using System.Collections.Generic;
using System.IO;

namespace bioscoop_sofa
{
    public class ExportOrderPlain : IExportOrderStrategy
    {
        public void Export(Order order)
        {
            var tickets = order.tickets;
            using (StreamWriter file = new StreamWriter("ExportText.txt"))
            {
                if (order.isStudentOrder)
                {
                    file.WriteLine("Student order");
                }
                else
                {
                    file.WriteLine("Non-student order");
                }

                file.WriteLine("");

                int index = 0;
                foreach (MovieTicket ticket in tickets)
                {
                    file.Write("Ticket nr. " + index);
                    file.Write(", Title: ");
                    file.Write(ticket.GetMovieScreening().GetMovie().GetTitle());
                    file.Write(", Price: ");
                    file.Write(order.CalculatePrice());
                    file.WriteLine("");
                    index++;
                }
            }
        }
    }
}