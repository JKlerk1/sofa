﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace bioscoop_sofa
{
    public class MovieTicket
    {
        [JsonProperty]
        private MovieScreening movieScreening;
        [JsonProperty]
        private int rowNr;
        [JsonProperty]
        private int seatNr;
        [JsonProperty]
        private bool isPremium;

        public MovieTicket(MovieScreening movieScreening, bool isPremiumReservation, int seatRow, int seatNr)
        {
            this.movieScreening = movieScreening;
            isPremium = isPremiumReservation;
            rowNr = seatRow;
            this.seatNr = seatNr;
        }

        public bool IsPremiumTicket()
        {
            return isPremium;
        }

        public MovieScreening GetMovieScreening()
        {
            return movieScreening;
        }

        public double Price { get { return movieScreening.GetPricePerSeat(); } }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
