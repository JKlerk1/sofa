﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace bioscoop_sofa
{
    public enum TicketExportFormat
    {
        PLAINTEXT,
        JSON
    }

    public class Order
    {
        private int orderNr;
        public bool isStudentOrder;
        public List<MovieTicket> tickets;
        private IExportOrderStrategy exportOrderBehaviour;
        private ICalcPriceStrategy calcPriceStrategy;

        public Order(int orderNr, bool isStudentOrder, TicketExportFormat format)
        {
            this.orderNr = orderNr;
            this.isStudentOrder = isStudentOrder;
            tickets = new List<MovieTicket>();
            exportOrderBehaviour = format == TicketExportFormat.JSON ? new ExportOrderJson() : new ExportOrderPlain();
            calcPriceStrategy = isStudentOrder ? new CalcPriceStudent() : new CalcPriceNonStudent();
        }

        public int GetOrderNr()
        {
            return orderNr;
        }

        public void AddSeatReservation(MovieTicket ticket)
        {
            tickets.Add(ticket);
        }

        public double CalculatePrice()
        {
            return calcPriceStrategy.CalcPrice(this);
        }

        public void Export()
        {
            exportOrderBehaviour.Export(this);
        }
    }
}
