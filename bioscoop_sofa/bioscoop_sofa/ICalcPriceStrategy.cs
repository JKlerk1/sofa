﻿namespace bioscoop_sofa
{
    public interface ICalcPriceStrategy
    {
        public double CalcPrice(Order order);
    }
}