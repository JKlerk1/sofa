﻿using System;

namespace bioscoop_sofa
{
    class Program
    {
        static void Main(string[] args)
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.Now.AddDays(1), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, true, 1, 1);
            Order order = new Order(0, true, TicketExportFormat.JSON);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            order.Export();

            Console.WriteLine("End");
            Console.ReadLine();
        }
    }
}
