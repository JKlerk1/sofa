﻿using System;
using System.Collections.Generic;

namespace bioscoop_sofa
{
    public class CalcPriceNonStudent : ICalcPriceStrategy
    {
        public double CalcPrice(Order order)
        {
            var tickets = order.tickets;
            double priceTotal = 0;
            List<DayOfWeek> daysFree = new List<DayOfWeek> { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday };
            List<DayOfWeek> daysWeekend = new List<DayOfWeek> { DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };

            int numOfWeekendTickets = 0;

            float index = 1;

            foreach (MovieTicket ticket in tickets)
            {
                double priceTicket = ticket.Price;
                bool isFreeTicket = false;

                if (index % 2 == 0)
                {
                    if (daysFree.Contains(ticket.GetMovieScreening().GetDateAndTime().DayOfWeek))
                    {
                        isFreeTicket = true;
                        priceTicket = 0;
                    }
                }

                if (daysWeekend.Contains(ticket.GetMovieScreening().GetDateAndTime().DayOfWeek))
                {
                    numOfWeekendTickets++;
                }

                if (!isFreeTicket && ticket.IsPremiumTicket())
                {
                    priceTicket += 3;
                }

                priceTotal += priceTicket;
                index++;
            }

            return numOfWeekendTickets >= 6 ? priceTotal * 0.9 : priceTotal;
        }
    }
}