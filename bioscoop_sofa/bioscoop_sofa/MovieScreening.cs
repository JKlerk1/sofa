﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace bioscoop_sofa
{
    public class MovieScreening
    {
        [JsonProperty]
        private Movie movie;
        private DateTime dateAndTime;
        private double pricePerSeat;

        public MovieScreening(Movie movie, DateTime dateAndTime, double pricePerSeat)
        {
            this.movie = movie;
            this.dateAndTime = dateAndTime;
            this.pricePerSeat = pricePerSeat;
        }

        public Movie GetMovie()
        {
            return movie;
        }

        public double GetPricePerSeat()
        {
            return pricePerSeat;
        }

        public DateTime GetDateAndTime()
        {
            return dateAndTime;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
