﻿using System.Collections.Generic;

namespace bioscoop_sofa
{
    public interface IExportOrderStrategy
    {
        public void Export(Order order);
    }
}