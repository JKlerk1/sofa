﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace bioscoop_sofa
{
    public class ExportOrderJson : IExportOrderStrategy
    {
        public void Export(Order order)
        {
            var tickets = order.tickets;
            using (StreamWriter file = File.CreateText("ExportJSON.json"))
            {
                JsonSerializer serializer = new JsonSerializer
                {
                    Formatting = Formatting.Indented
                };
                //serialize object directly into file stream
                serializer.Serialize(file, tickets);
            }
        }
    }
}