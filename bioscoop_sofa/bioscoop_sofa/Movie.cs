﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace bioscoop_sofa
{
    public class Movie
    {
        private List<MovieScreening> movieScreenings;
        [JsonProperty]
        private string title;

        public Movie(string title)
        {
            this.title = title;
            movieScreenings = new List<MovieScreening>();
        }

        public void AddScreening(MovieScreening screening)
        {
            movieScreenings.Add(screening);
        }

        public string GetTitle()
        {
            return title;
        }

        public List<MovieScreening> GetMovieScreenings()
        {
            return movieScreenings;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
