﻿using System;
using System.Collections.Generic;

namespace bioscoop_sofa
{
    public class CalcPriceStudent : ICalcPriceStrategy
    {
        public double CalcPrice(Order order)
        {
            var tickets = order.tickets;
            double priceTotal = 0;

            float index = 1;

            foreach (MovieTicket ticket in tickets)
            {
                double priceTicket = ticket.Price;
                bool isFreeTicket = false;

                if (index % 2 == 0)
                {
                    isFreeTicket = true;
                    priceTicket = 0;
                }

                if (!isFreeTicket && ticket.IsPremiumTicket())
                {
                    priceTicket += 2;
                }

                priceTotal += priceTicket;
                index++;
            }
            return priceTotal;
        }
    }
}