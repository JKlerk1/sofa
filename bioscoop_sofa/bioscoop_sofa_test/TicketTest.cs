using System;
using System.Globalization;
using bioscoop_sofa;
using Xunit;

namespace bioscoop_sofa_test
{
    public class TicketTest
    {
        // 1-2-8
        [Fact]
        public void Test1()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.Now.AddDays(1), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, true, TicketExportFormat.JSON);
            var expectedPrice = ticket.Price;
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            Assert.True(order.CalculatePrice() == expectedPrice);
        }

        // 1-2-9
        [Fact]
        public void Test2()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("05/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, false, TicketExportFormat.JSON);
            var expectedPrice = (ticket.Price * 6) * 0.9;
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            var total = order.CalculatePrice();

            Assert.True(total == expectedPrice);
        }

        // 1-3-4-8
        [Fact]
        public void Test3()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("05/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, true, TicketExportFormat.JSON);
            var expectedPrice = ticket.Price;
            order.AddSeatReservation(ticket);


            Assert.True(order.CalculatePrice() == expectedPrice);
        }

        // 1-3-4-9
        [Fact]
        public void Test4()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("05/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, false, TicketExportFormat.JSON);
            var expectedPrice = (ticket.Price * 6) * 0.9;
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            var total = order.CalculatePrice();

            Assert.True(total == expectedPrice);
        }

        // 1-3-5-6-8
        [Fact]
        public void Test5()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("07/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, true, TicketExportFormat.JSON);
            var expectedPrice = ticket.Price;
            order.AddSeatReservation(ticket);

            Assert.True(order.CalculatePrice() == expectedPrice);
        }

        // 1-3-5-6-9
        [Fact]
        public void Test6()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("07/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, false, 1, 1);
            Order order = new Order(0, false, TicketExportFormat.JSON);
            var expectedPrice = (ticket.Price * 3);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            var total = order.CalculatePrice();

            Assert.True(total == expectedPrice);
        }

        // 1-3-5-7-8
        [Fact]
        public void Test7()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("07/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, true, 1, 1);
            Order order = new Order(0, false, TicketExportFormat.JSON);
            var expectedPrice = ticket.Price + 3;
            order.AddSeatReservation(ticket);


            var total = order.CalculatePrice();
            Assert.True(total == expectedPrice);
        }

        // 1-3-5-7-9
        [Fact]
        public void Test8()
        {
            Movie movie = new Movie("Spydermens");
            MovieScreening movieScreening = new MovieScreening(movie, DateTime.ParseExact("07/02/2022", "dd/MM/yyyy", CultureInfo.InvariantCulture), 20);
            MovieTicket ticket = new MovieTicket(movieScreening, true, 1, 1);
            Order order = new Order(0, true, TicketExportFormat.JSON);
            var expectedPrice = (ticket.Price + 2) * 3;
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);
            order.AddSeatReservation(ticket);

            var total = order.CalculatePrice();

            Assert.True(total == expectedPrice);
        }
    }
}